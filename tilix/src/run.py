#! /usr/bin/python3
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

settings = Gtk.Settings.get_default()
settings.set_property('gtk-application-prefer-dark-theme', True)

builder = Gtk.Builder.new_from_file('widgets.ui')

user_popover = builder.get_object('user_popover')

window = builder.get_object('window')
window.connect('destroy', lambda p: Gtk.main_quit())
window.connect('button_press_event', lambda p, b: user_popover.popup())
window.show_all()

Gtk.main()
