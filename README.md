
# Third Party App Mockups

A place for non-core app mockups, to help keep the [app-mockups repo](https://gitlab.gnome.org/Teams/Design/app-mockups) from exploding in size.
